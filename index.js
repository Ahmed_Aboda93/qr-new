/**
 * @format
 */

import {AppRegistry} from 'react-native';
import Root from './src/app/Root';
import {name as OriginallyGuaranteed} from './app.json';

AppRegistry.registerComponent(OriginallyGuaranteed, () => Root);
