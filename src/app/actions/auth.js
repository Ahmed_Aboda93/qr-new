import axios from "axios";
import { AsyncStorage } from "react-native";
import  Config  from "../common/Config";
import {
  LOGIN_ATTEMPT,
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  LOGOUT,
  CREATE_ACCOUNT_ATTEMPT,
  CREATE_ACCOUNT_SUCCESS,
  CREATE_ACCOUNT_FAILED,
  RESET_AUTH_LOADING,
} from "./ActionTypes";



// Login Action >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
export const login = (email,password) => {
    return dispatch => {
      dispatch({ type: LOGIN_ATTEMPT });
      axios({
        method: "post",
        url: `${Config.baseUrl}/auth/login`,
        data: {
          email     : email,
          password  : password
        }
      })
        .then(response => {
          console.log(response);
          if (response.status == 200) {
              let token = Config.token_type + response.data.access_token;
  
              console.log(token);
  
              AsyncStorage.setItem('@token',token);
              dispatch({
                type: LOGIN_SUCCESS,
                payload: { token }
              });
            
          } else {
            dispatch({
              type: LOGIN_FAILED,
              payload: "There was an error connecting to the server"
            });
          }
        })
        .catch(function(error) {
          console.log(error);
          dispatch({
            type: LOGIN_FAILED,
            payload: "There was an error connecting to the server"
          });
        });
    };
  };

//Create Account Action >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

export const signup = (name, email, password ,country,gender,phone,birth_date) => {
  console.log( name, email, password ,country,gender,phone,birth_date);
  return dispatch => {
    dispatch({ type: CREATE_ACCOUNT_ATTEMPT });
    axios({
      method: "post",
      url: `${Config.baseUrl}/auth/register`,
      data: {
        name        : name,
        email       : email,
        password    : password,
        country     : country,
        gender      : gender,
        phone       : phone,
        birth_date  : birth_date
      }
    })
      .then(response => {
        console.log(response);
        if (response.status == 200) {
          let token = Config.token_type + response.data.access_token;

            console.log(token);

            AsyncStorage.setItem('@token',token);
            console.log("success");
            dispatch({
              type: CREATE_ACCOUNT_SUCCESS,
              payload: { token }
            });
        } else {
          dispatch({
            type: CREATE_ACCOUNT_FAILED,
            payload: "Please enter the required data correctly"
          });
        }
      })
      .catch(function(error) {
        console.log(error);
        dispatch({
          type: CREATE_ACCOUNT_FAILED,
          payload: "Can't connect with this data"
        });
      });
  };
};

  // Reset Auth >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

export const resetAuth = () => {
  return dispatch => {
    dispatch({ type: RESET_AUTH_LOADING });
  }
};


export const userLogout = () => {
  return dispatch => {
    dispatch({ type: LOGOUT });
  }
};