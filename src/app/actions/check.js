import axios from "axios";
import Config from "../common/Config";
import {
  CHECK_ATTEMPT,
  CHECK_SUCCESS,
  CHECK_FAILED
} from "./ActionTypes";



// Login Action >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
export const check = (code, token) => {
  return dispatch => {
    dispatch({ type: CHECK_ATTEMPT });
    axios({
      method: "post",
      url: `${Config.baseUrl}/validate`,
      headers: {
        Authorization: token
      },
      data: {
        code: code,
      },

    })
      .then(response => {
        if (response.status == 200) {
            let message = response.data.message;
            let status = response.data.status;
            if (status == 'valid'){
              console.log(status);
              let title = response.data.title;
              let brand = response.data.brand;
              let origin = response.data.origin;
              let specs = response.data.specs;
              let img = response.data.img;
              console.log(brand);
              dispatch({
                type: CHECK_SUCCESS,
                payload: { message, status, title, brand, origin, specs, img }
              });
            } else {
              console.log(status);
              dispatch({
                type: CHECK_SUCCESS,
                payload: { message, status }
              });
            }
        } 
      })
      .catch(function (error) {
        console.log("invalid last");
        console.log(error);
        let message = "Invalid request";
        let status = "Invalid request";
        console.log(status);
        dispatch({
          type: CHECK_FAILED,
          payload: { message, status }
        });
      });
  };
};

