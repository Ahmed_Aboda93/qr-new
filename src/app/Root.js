import React from 'react'
import { StatusBar,AsyncStorage} from 'react-native'
import {Provider} from 'react-redux'
import {store,persisedtStore} from './store';
import App from './App'

import {PersistGate} from 'redux-persist/integration/react';

class Root extends React.Component {
  constructor(props) {
    super(props)
    StatusBar.setBarStyle('light-content', true,);
  }
  
  render() {
    return (
      <Provider store={store}>
         <PersistGate persistor={persisedtStore}>
            <App />
         </PersistGate>
      </Provider>
    )
  }
}

export default Root





