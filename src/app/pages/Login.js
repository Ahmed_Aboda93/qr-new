import React, { Component } from 'react';
import { View, StyleSheet,ActivityIndicator } from 'react-native';
import {Input,Label,Form,Item, Button,Text} from 'native-base';
import * as AuthActions from "../actions/auth";
import Colors from '../common/Colors';

import { connect } from "react-redux";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password:'',
    };
  }

  onLogin() {
      const { email, password } = this.state;
      this.props.login( email, password);
  }
  componentDidMount() {
    this.props.resetAuth()
  }

  render() {
    return (
      <View>
          <View style={{padding: 20}} >
            <Form>
                <Item floatingLabel style={styles.itemStyle}>
                    <Input style={styles.inputStyle} placeholder='Email' 
                    onChangeText={(email) => this.setState({ email })} />
                </Item>
                <Item floatingLabel style={styles.itemStyle}>
                    <Input style={styles.inputStyle} secureTextEntry={true}  placeholder='Password' 
                    onChangeText={(password) => this.setState({ password })} />
                </Item>
                {(this.props.user_login_loading)?( <ActivityIndicator />):(null)}
                <Text style={styles.ErrorMessage}>{this.props.user_login_error_message}</Text>
                <Button 
                    style={styles.button}
                    block 
                    onPress={this.onLogin.bind(this)}
                >
                    <Text style={styles.textButton}>Login</Text>
                </Button>
                <Button 
                    style={styles.button2}
                    block 
                    onPress={() => this.props.navigation.navigate('Register')}
                >
                    <Text style={styles.textButton2}>No Account ? Signup</Text>
                </Button>
            </Form>
          </View>
      </View>
    );
  }


  componentWillReceiveProps(nextProps){
    if(nextProps.user_login_success){
         this.props.navigation.navigate('AppDrawer')
    }
  }

}


function mapStateToPrpos({auth}) {
  return {
    user_login_loading :auth.user_login_loading,
    user_login_error_message :auth.user_login_error_message,
    user_login_success:auth.user_login_success,

  };
}

function mapDispatchToPropos(dispatch) {
  return {
    login:(email,password) =>dispatch(AuthActions.login(email,password)),    
    resetAuth: () => dispatch(AuthActions.resetAuth()),
  };
}

export default connect(
  mapStateToPrpos,
  mapDispatchToPropos
)(Login);

const styles = StyleSheet.create({
    container: {
      backgroundColor: Colors.white,
      flex: 1,
      alignItems: 'center',
    },
    itemStyle: {
        padding: 0,
        marginLeft: 0
    },
    inputStyle: {
        marginBottom: 10
    },
    button: {
      backgroundColor:Colors.DarkGray,
      marginBottom: 15
    },
    button2: {
      backgroundColor: Colors.white,
      borderColor:Colors.DarkGray,
      borderWidth: 2
    },
    textButton: {
      color: Colors.white
    },
    textButton2: {
      color: Colors.DarkGray
    },
    ErrorMessage: {
      fontSize: 14,
      padding: 24,
      color: Colors.Red,
      fontWeight: '400'
    }
  })
  