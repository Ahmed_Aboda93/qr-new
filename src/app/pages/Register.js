import React, { Component } from 'react';
import { View ,ScrollView, StyleSheet, ActivityIndicator, Modal,TouchableOpacity,Alert } from 'react-native';
import {Input,Label,Form,Item, Button,Text,DatePicker,Picker,Icon,Container} from 'native-base';
import { CountrySelection } from 'react-native-country-list';
import * as AuthActions from "../actions/auth";

var moment = require('moment');
import Colors from '../common/Colors';
import { connect } from "react-redux";

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      phone: '',
      country: 'Select Country',
      gender: "male",
      birth_date: '',
      password:'',
      modalVisible: false,
    };
    this.setDate = this.setDate.bind(this);
  }
  componentDidMount() {
    this.props.resetAuth()
  }
  onSignUp() {
      const { name, email, password ,country,gender,phone,birth_date} = this.state;
      console.log( name, email, password ,country,gender,phone,birth_date);
      this.props.signup( name, email, password,country,gender,phone,birth_date);
  }
  setDate(newDate) {
    const BirthDate = moment(newDate).format("YYYY-MM-DD");
    this.setState({ birth_date: BirthDate});
  }
  onValueChange(value) {
    this.setState({
      gender: value
    });
  }
  onCountrySelection(item) {
    this.setModalVisible(!this.state.modalVisible)
    console.log(item);
    this.setState({
      country: item.name
    });
    console.log(this.state.country);
  }
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }
  render() {
    return (
      <Container style={styles.container}>
      <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.setModalVisible(!this.state.modalVisible);
          }}>
          <View style={{marginTop: 22}}>
            <View style={{padding: 20}}>
              <Button 
                    style={styles.button}
                    block 
                    onPress={() => {
                      this.setModalVisible(!this.state.modalVisible);
                    }}
                >
                    <Text style={styles.textButton}>Close</Text>
                </Button>
            </View>
            <ScrollView>
              <CountrySelection action={(item) => this.onCountrySelection(item)} selected={this.state.country}/>
            </ScrollView>
          </View>
        </Modal>
        <ScrollView style={styles.content} >
          <Form>
                <Item floatingLabel style={styles.itemStyle}>
                    <Input style={styles.inputStyle} placeholder='Name' 
                    onChangeText={(name) => this.setState({ name })} />
                </Item>
                <Item floatingLabel style={styles.itemStyle}>
                    <Input style={styles.inputStyle} placeholder='Email' 
                    onChangeText={(email) => this.setState({ email })} />
                </Item>
                
                <Item floatingLabel style={styles.itemStyle}>
                    <Input style={styles.inputStyle} secureTextEntry={true}  placeholder='Password' 
                    onChangeText={(password) => this.setState({ password })} />
                </Item>
                <Item floatingLabel style={styles.itemStyle}>
                    <Input style={styles.inputStyle} placeholder='Phone' keyboardType={'number-pad'}
                    onChangeText={(phone) => this.setState({ phone })} />
                </Item>
                <Item fixedLabel style={styles.pickerItemStyle}>
                  <Label>Country</Label>
                  <TouchableOpacity
                  style={styles.pickerStyle}
                    onPress={() => {
                      this.setModalVisible(true);
                    }}>
                    <Text>{this.state.country}</Text>
                  </TouchableOpacity>
                </Item>
                <Item fixedLabel style={styles.pickerItemStyle}>
                    <Label>Gender</Label>
                    <View style={styles.pickerCont}>
                      <Picker
                          mode="dropdown"
                          iosHeader="Gender"
                          iosIcon={<Icon name="arrow-down" />}
                          style={styles.pickerStyle}
                          itemStyle={styles.pickerItem}
                          selectedValue={this.state.gender}
                          onValueChange={this.onValueChange.bind(this)}
                          >
                          <Picker.Item label="Male" value="male" />
                          <Picker.Item label="Female" value="female" />
                      </Picker>
                    </View>
                </Item>
                <Item style={styles.datePickerItemStyle}>
                    <Label>Birth Date</Label>
                    <DatePicker
                        defaultDate={new Date(1950, 4, 4)}
                        minimumDate={new Date(1950, 1, 1)}
                        maximumDate={new Date(2040, 12, 31)}
                        locale={"en"}
                        formatChosenDate={date => {return moment(date).format('YYYY-MM-DD');}}
                        timeZoneOffsetInMinutes={undefined}
                        modalTransparent={false}
                        animationType={"fade"}
                        androidMode={"default"}
                        placeHolderText="Select date"
                        textStyle={{ color: Colors.Black }}
                        placeHolderTextStyle={{ color: Colors.AppColor }}
                        onDateChange={this.setDate}
                        disabled={false}
                    />
                </Item>
                {(this.props.create_user_loading)?( <ActivityIndicator />):(null)}
                <Text style={styles.ErrorMessage}>{this.props.create_user_error_message}</Text>
                <Button 
                    style={styles.button}
                    block 
                    onPress={this.onSignUp.bind(this)}
                >
                    <Text style={styles.textButton}>Register</Text>
                </Button>
          </Form>
        </ScrollView>
      </Container>
    );
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.crate_user_success) {
      this.props.navigation.navigate('AppDrawer')
    }
  }
  
}

function mapStateToPrpos({auth}) {
  return {
    create_user_loading       : auth.create_user_loading,
    crate_user_success        : auth.crate_user_success,
    create_user_error_message : auth.create_user_error_message,

  };
}

function mapDispatchToPropos(dispatch) {
  return {
    signup:(name,email,password,country,gender,phone,birth_date) =>dispatch(AuthActions.signup(name,email,password,country,gender,phone,birth_date)),    
    resetAuth: () => dispatch(AuthActions.resetAuth()),
  };
}

export default connect(
  mapStateToPrpos,
  mapDispatchToPropos
)(Register);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    
  },
  content: {
    paddingHorizontal: 20
  },
  itemStyle: {
    padding: 0,
    marginLeft: 0,
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: Colors.LightGray,
  },
  inputStyle: {
    marginBottom: 10
  },
  button: {
    backgroundColor:Colors.DarkGray,
    marginBottom: 15
  },
  textButton: {
    color: Colors.white
  },
  datePickerItemStyle: {
    borderBottomWidth: 0,
    justifyContent: 'space-between',
    marginLeft: 0,
    marginRight: 0
  },
  pickerItemStyle: {
    borderBottomWidth: 0,
    justifyContent: 'space-between',
    marginLeft: 0,
    marginRight: 0,
    marginTop: 15,
  },
  pickerStyle: {
    marginLeft: 0,
    width: 110,
  },
  ErrorMessage: {
    fontSize: 14,
    padding: 24,
    color: Colors.Red,
    fontWeight: '400'
  }
})


