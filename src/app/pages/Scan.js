import React, { PureComponent  } from 'react';
import { View, Text ,StyleSheet,Alert,TouchableOpacity,SafeAreaView} from 'react-native';
import { RNCamera } from 'react-native-camera';
import * as AuthkActions from '../actions/auth';
import * as CheckActions from '../actions/check';
import { connect } from 'react-redux';
import Colors from '../common/Colors';

import IconComponent from 'react-native-vector-icons/Ionicons'; 

 class Scan extends PureComponent  {
  
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  barcodeRecognized = ({ barcodes }) => {
     barcodes.forEach(barcode => 
      
      this.props.navigation.navigate('result',{'data':barcode.data})
      
      )
      
     barcodes.forEach(barcode => this.callApi(barcode))
     
  };
  callApi(barcode){
    this.props.check(barcode.data, this.props.token);
    console.log(barcode.data);
    console.log(this.props.token);
  }
  
  componentDidMount() {
    console.log("hi you are on scan screen");
    const { navigation } = this.props;
    navigation.addListener('willFocus', () => {
      this.setState({ focusedScreen: true })
      console.log("hi you are on focusedScreen");
    }
    );
    navigation.addListener('willBlur', () => {
      this.setState({ focusedScreen: false })
      console.log("hi you are leaved focusedScreen");
    }
    );
  }
  render() {
    const { hasCameraPermission, focusedScreen } = this.state;
    if (hasCameraPermission === null) {
      return <View />;
    } else if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    } else if (focusedScreen){
      return (
        <View style={styles.container}>
          <SafeAreaView></SafeAreaView>
          <TouchableOpacity style={styles.iconCont} onPress={() => this.props.navigation.goBack()}>
            <IconComponent
              name='ios-arrow-back'
              style={styles.icon}
            />
            <Text style={styles.iconText}>Back</Text>
          </TouchableOpacity>
          <RNCamera
            ref={ref => {
              this.camera = ref;
            }}
            style={{
              flex: 1,
              width: '100%',
            }}
            onGoogleVisionBarcodesDetected={this.barcodeRecognized}
          >
          </RNCamera>
  
        </View>
      );
    } else {
      return <View />;
    }
  }
}

function mapStateToProps({ check, auth }) {
  return {
    check_loading: check.check_loading,
    check_success: check.check_success,
    check_error_message: check.check_error_message,
    message: check.message,
    status: check.status,
    token: auth.token,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    check: (code, token) => dispatch(CheckActions.check(code, token))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Scan);



const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  iconCont: {
    backgroundColor: Colors.black54,
    paddingHorizontal: 15,
    position: 'absolute',
    alignSelf: 'auto',
    alignItems: 'center',
    left: 10,
    top: 50,
    zIndex: 1,
  },
  icon: {
    fontSize: 28,
    fontWeight: 'bold',
    color: Colors.white
  },
  iconText: {
    color: Colors.white
  }
});

