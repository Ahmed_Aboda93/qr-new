import React, { Component } from 'react';
import { StyleSheet, View, ActivityIndicator,TouchableOpacity,FlatList,Image,ScrollView,SafeAreaView } from 'react-native';
import {Icon,Button,Text, Thumbnail, Container} from 'native-base';

import IconComponent from 'react-native-vector-icons/Ionicons'; 
import Colors from '../common/Colors';

import axios from "axios";
import * as AuthkActions from '../actions/auth';
import  Config  from "../common/Config";
import { connect } from 'react-redux';

class SingleCode extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      title: '',
      brand: '',
      origin: '',
      specs: '',
      img: '',
    };
  }

  componentDidMount() {
    this.getItem()
  }
  getItem() {
    const token = this.props.token;
    const { navigation } = this.props;
    const itemId = navigation.getParam('itemId');
    axios({
      method: "post",
      url: `${Config.baseUrl}/item-details`,
      headers: {
        Authorization: token
      },
      data: {
        item_id: itemId
      }
    })
    .then(response => {
      if (response.status == 200) {
        let title = response.data.title;
        let brand = response.data.brand;
        let origin = response.data.origin;
        let specs = response.data.specs;
        let img = response.data.img;
        this.setState({
          isLoading: false,
          title: title,
          brand: brand,
          origin: origin,
          specs: specs,
          img: img,
        })
      } 
    })
    .catch(function (error) {
      console.log(error)
    });
  }
  toggleDrawerClick = () => {
    this.props.navigation.toggleDrawer();
  };

  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.activity}>
          <ActivityIndicator size="large" color={Colors.AppColor} />
        </View>
      )
    }
    return (
      <Container>
        <SafeAreaView></SafeAreaView>
         <TouchableOpacity style={styles.iconCont} onPress={() => this.props.navigation.goBack()}>
          <IconComponent
            name='ios-arrow-back'
            style={styles.icon}
          />
        </TouchableOpacity>
        <View style={styles.container}>
        <ScrollView>
            <View style={styles.detailsCont}> 
              <View style={styles.imageCont}>
                <Image style={styles.imageStyle3} resizeMode={'contain'} source={{ uri: this.state.img }} />
              </View>

              <View style={styles.textCont}>
                <Text style={styles.titleStyle}>Title :  </Text>
                <Text style={styles.valueStyle}>{this.state.title}</Text>
              </View>

              <View style={styles.textCont}>
                <Text style={styles.titleStyle}>Brand :  </Text>
                <Text style={styles.valueStyle}>{this.state.brand}</Text>
              </View>
              <View style={styles.textCont}>
                <Text style={styles.titleStyle}>Origin :   </Text>
                <Text style={styles.valueStyle}>{this.state.origin}</Text>
              </View>
              {this.state.specs != ''
                ?
                <View style={styles.textCont2}>
                  <Text style={styles.titleStyle}>Specs :  </Text>
                  <Text style={styles.valueStyle}>{this.state.specs}</Text>
                </View>
                : <View />
              }
            </View>
          </ScrollView>
        </View>
      </Container>
    );
  }
}

function mapStateToProps({ auth }) {
  return {
    token: auth.token,
  };
}

export default connect(
  mapStateToProps
)(SingleCode);

const styles = StyleSheet.create({
  
  container: {
    backgroundColor: Colors.white,
    padding: 15,
  },
  textCont: {
    flexDirection: 'row',
    marginBottom: 10
  },
  textCont2: {
    flexDirection: 'column',
    marginBottom: 10
  },
  titleStyle: {
    fontSize: 18,
    color: Colors.DarkGray,
    marginBottom: 10
  },
  valueStyle: {
    color: Colors.Gray
  },
  imageCont: {
    alignItems: 'center',
  },
  imageStyle3: {
    width: 200,
    height: 200,
    margin: 20
  },
  button: {
    backgroundColor:Colors.DarkGray,
    margin: 25,
  },
  iconCont: {
    alignItems: 'flex-start',
    padding: 15
  },
  icon: {
    fontSize: 28,
    marginRight: 15,
    fontWeight: 'bold',
    color: Colors.black54
  },
  activity: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
})

