import React, { Component } from 'react';
import { StyleSheet, View, ActivityIndicator,TouchableOpacity ,SafeAreaView} from 'react-native';
import {Icon,Button,Text, Thumbnail, Container} from 'native-base';

import { RNCamera } from 'react-native-camera';
import IconComponent from 'react-native-vector-icons/Ionicons'; 
import Colors from '../common/Colors';

export default class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      
    };
  }
  
  barcodeRecognized = ({ barcodes }) => {
    barcodes.forEach(barcode => console.log(barcode.data))
  };
  toggleDrawerClick = () => {
    //Props to open/close the drawer
    this.props.navigation.toggleDrawer();
  };

  render() {
    return (
      <Container>
        <SafeAreaView backgroundColor={Colors.white}></SafeAreaView>
         <TouchableOpacity style={styles.iconCont} onPress={() => this.props.navigation.toggleDrawer()}>
          <IconComponent
            name='ios-menu'
            style={styles.icon}
          />
        </TouchableOpacity>
        <View style={styles.container}>
          <Thumbnail style={styles.imageStyle} large square source={require('../../../assets/icons/logo-gold.png')} />
          
          <Text style={styles.textStyle}> Scan your OG code </Text>
          <Button 
              style={styles.button}
              block
              onPress={() => this.props.navigation.navigate('Scan')}
              >
            <Text>Scan</Text>
          </Button>
        </View>
        
      </Container>
    );
  }
}


const styles = StyleSheet.create({
  
  container: {
    backgroundColor: Colors.white,
    alignItems: 'center',
  },
  textStyle: {
    fontSize: 18,
    color: Colors.DarkGray,
    margin: 20,
  },
  button: {
    backgroundColor:Colors.DarkGray,
    margin: 25,
  },
  iconCont: {
    alignSelf: 'flex-start',
    padding: 15,
    position: 'relative',
    width: 'auto',
  },
  icon: {
    fontSize: 24,
    fontWeight: 'bold',
    color: Colors.black54
  },
  imageStyle: {
    width: 180,
    height: 180,
    margin: 'auto',
    marginTop: '20%',
  }
})

