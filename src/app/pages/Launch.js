import React, { Component } from 'react';
import { View, Text, StyleSheet,Image,AsyncStorage,ScrollView,ActivityIndicator } from 'react-native';

import Colors from '../common/Colors';

class Launch extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    this.loadApp();
  }
  loadApp = async () => {
    const token = await AsyncStorage.getItem('@token');
    this.props.navigation.navigate(token ? 'AppDrawer' : 'AuthStack')
  }
  render() {
    return (
    <View style={styles.container}>
        <ScrollView contentContainerStyle={styles.content}>
            <Image style={styles.imageStyle} source={require('../../../assets/icons/logo-gold.png')} />
    
            <ActivityIndicator color={Colors.AppColor} />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container:{
      flex:1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: Colors.white,
    },
    content:{
      justifyContent: 'center',
      alignItems: 'center',
      flex:1
    },
    imageStyle: {
        width: 200,
        height: 200,
        marginTop: '40%'
    }
   
  })

export default Launch;
