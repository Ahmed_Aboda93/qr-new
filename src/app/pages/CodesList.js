import React, { Component } from 'react';
import { StyleSheet, View, ActivityIndicator,TouchableOpacity,FlatList,Image ,SafeAreaView} from 'react-native';
import {Icon,Button,Text, Thumbnail, Container} from 'native-base';

import IconComponent from 'react-native-vector-icons/Ionicons'; 
import Colors from '../common/Colors';

import axios from "axios";
import * as AuthkActions from '../actions/auth';
import  Config  from "../common/Config";
import { connect } from 'react-redux';

class CodesList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      refreshing: false,
      notFoundMessage: '',
      status: '',
      items: []
    };
  }

  componentDidMount() {
    this.getItems()
  }
  getItems() {
    const token = this.props.token;
    axios({
      method: "get",
      url: `${Config.baseUrl}/list-items`,
      headers: {
        Authorization: token
      }
    })
    .then(response => {
      if (response.status == 200) {
        let message = response.data.message;
        let status = response.data.status;
        if(status == 'not found'){
          this.setState({
            status: status,
            notFoundMessage: message,
            isLoading: false,
          })
        }else{
          let items = response.data;
          this.setState({
            status: null,
            isLoading: false,
            refreshing: false,
            items: items
          })
          
        }
      } 
    })
    .catch(function (error) {
      console.log(error)
    });
  }
  toggleDrawerClick = () => {
    this.props.navigation.toggleDrawer();
  };

  handleRefresh = () => {
    this.setState(
      {
        refreshing: true
      }, () => {
        this.getItems();
      }
  )
  }
  render() {
    if(this.state.isLoading){
      return(
        <View style={styles.activity}>
          <ActivityIndicator size="large" color={Colors.AppColor} />
        </View>
      )
    }
    return (
      <Container>
        <SafeAreaView></SafeAreaView>
         <TouchableOpacity style={styles.iconCont} onPress={() => this.props.navigation.toggleDrawer()}>
          <IconComponent
            name='ios-menu'
            style={styles.icon}
          />
        </TouchableOpacity>
        <View style={styles.container}>
        {this.state.status == "not found"
          ?
          <Text style={styles.message}>{this.state.notFoundMessage}</Text>
          : <View />
        }
        {this.state.status == null
          ?
          <View>
            <Text style={styles.swipe}>Swipe down to refresh</Text>
            <FlatList 
                data={this.state.items}
                style={styles.itemsList}
                refreshing= {this.state.refreshing}
                onRefresh= {this.handleRefresh}
                renderItem={({ item }) => (
                  <TouchableOpacity 
                  style={styles.item} 
                  key={item.id} 
                  onPress={() => {
                    this.props.navigation.navigate('SingleCode', {
                        itemId: `${item.id}`,
                      });
                  }}
                    >
                    <Image style={styles.itemImage} source={{uri: item.img}}/>
                    <Text style={styles.itemTitle}>{item.title}</Text>
                  </TouchableOpacity>
                )}
                keyExtractor={(item, index) => item.id.toString()}
            />
          </View>
          : <View />
        }
          
        </View>
      </Container>
    );
  }
}
function mapStateToProps({ auth }) {
  return {
    token: auth.token,
  };
}

export default connect(
  mapStateToProps
)(CodesList);

const styles = StyleSheet.create({
  
  container: {
    backgroundColor: Colors.white,
    padding: 15,
  },
  textStyle: {
    fontSize: 18,
    color: Colors.DarkGray,
    margin: 20,
  },
  button: {
    backgroundColor:Colors.DarkGray,
    margin: 25,
  },
  iconCont: {
    alignItems: 'flex-start',
    padding: 15
  },
  icon: {
    fontSize: 24,
    marginRight: 15,
    fontWeight: 'bold',
    color: Colors.black54
  },
  itemsList: {
    height: '100%'
  },
  item: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    borderColor: Colors.LightGray,
    borderWidth: 1,
    borderRadius: 5,
    padding: 10,
    marginBottom: 10
  },
  itemImage: {
    width: 50,
    height: 50,
    borderColor: Colors.LightGray,
    borderWidth: 1,
    borderRadius: 50/2,
    marginRight: 10,
  },
  swipe: {
    textAlign: 'center',
    marginBottom: 20,
    color: Colors.Gray
  },
  imageStyle: {
    width: 180,
    height: 180,
    margin: 'auto',
    marginTop: '20%',
  },
  message: {
    padding: 10,
    borderRadius: 5,
    borderColor: Colors.LightGray,
    borderWidth: 1,
    textAlign: 'center',
  },
  activity: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
})

