import React, { Component } from 'react';
import { View, StyleSheet, AsyncStorage, ScrollView,ActivityIndicator,Image,TouchableOpacity,SafeAreaView} from 'react-native';
import { Button, Text, Thumbnail, Container } from 'native-base';
import * as AuthkActions from '../actions/auth';
import * as CheckActions from '../actions/check';
import Colors from '../common/Colors';
import { NavigationEvents } from 'react-navigation';
import IconComponent from 'react-native-vector-icons/Ionicons'; 
import { connect } from 'react-redux';

class result extends Component {

  constructor(props) {
    super(props);
    this.state = {
    };

  }
  toggleDrawerClick = () => {
    this.props.navigation.toggleDrawer();
  };
  render() {
    return (
      <Container>
        <SafeAreaView></SafeAreaView>
        <TouchableOpacity style={styles.iconCont} onPress={() => this.props.navigation.toggleDrawer()}>
          <IconComponent
            name='ios-menu'
            style={styles.icon}
          />
        </TouchableOpacity>
        <ScrollView>
          <View style={styles.container}>
            <Text style={styles.textStyle} >Your OG Result is  : </Text>

            {(this.props.check_loading)?( <ActivityIndicator />):(null)}
            
            {this.props.status == "valid"
              ?
                <View style={styles.itemContainer2}>
                  <View style={styles.topCont}>
                    <Thumbnail style={styles.imageStyle2} large square source={require('../../../assets/icons/logo-gold.png')} />
                    <Text style={styles.textStyle}>{this.props.message}</Text>
                    <Image style={styles.imageStyle3} resizeMode={'contain'} source={{ uri: this.props.img }} />
                  </View>
                  <View style={styles.detailsCont}> 
                    <Text style={styles.textStyle}>Title :
                      <Text style={styles.valueStyle}>
                        {this.props.title}
                      </Text>
                    </Text>
                    <Text style={styles.textStyle}>Brand :  
                      <Text style={styles.valueStyle}>
                        {this.props.brand}
                      </Text>
                    </Text>
                    <Text style={styles.textStyle}>Origin :  
                      <Text style={styles.valueStyle}>
                        {this.props.origin}
                      </Text>
                    </Text>
                    <Text style={styles.textStyle}>Specs :  
                      <Text style={styles.valueStyle}>
                        {this.props.specs}
                      </Text>
                    </Text>
                  </View>
                </View>
              : <View />
            }

            {this.props.status == "invalid"
              ?
                <View style={styles.itemContainer}>
                  <Thumbnail style={styles.imageStyle} large square source={require('../../../assets/icons/error.png')} />
                  <Text style={styles.textStyle2}>{this.props.message}</Text>
                </View>
              : <View />
            }


            {this.props.status == "repeated"
              ?
                <View style={styles.itemContainer}>
                  <Thumbnail style={styles.imageStyle} large square source={require('../../../assets/icons/warning.png')} />
                  <Text style={styles.textStyle2}>{this.props.message}</Text>
                </View>
              : <View />
            }
            {this.props.status == "Invalid request"
              ?
                <View style={styles.itemContainer}>
                  <Thumbnail style={styles.imageStyle} large square source={require('../../../assets/icons/error.png')} />
                  <Text style={styles.textStyle2}>{this.props.message}</Text>
                </View>
              : <View />
            }
            <Button
              style={styles.button}
              block
              onPress={() => this.props.navigation.navigate('Scan')}
            >
              <Text>Scan Again </Text>
            </Button>

          </View>
        </ScrollView>
      </Container>
    );
  }
}

function mapStateToProps({ check, auth }) {
  return {
    check_loading: check.check_loading,
    check_success: check.check_success,
    check_error_message: check.check_error_message,
    message: check.message,
    status: check.status,
    title: check.title,
    brand: check.brand,
    origin: check.origin,
    specs: check.specs,
    img: check.img,
    token: auth.token,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    check: (code, token) => dispatch(CheckActions.check(code, token))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(result);

const styles = StyleSheet.create({

  container: {
    backgroundColor: Colors.white,
    flex: 1,
    paddingTop: 15,
    width: "100%",
    alignItems: 'center',
  },
  itemContainer: {
    alignItems: 'center',
  },
  itemContainer2: {
    flex: 1,
    width: "100%",
    alignItems: 'flex-start',
  },
  topCont: {
    flex: 1,
    width: "100%",
    alignItems: 'center',
  },
  detailsCont:{
    alignItems: 'flex-start',
    paddingHorizontal: 10
  },
  textStyle: {
    fontSize: 18,
    color: Colors.DarkGray,
    margin: 10,
  },
  textStyle2: {
    fontSize: 18,
    color: Colors.DarkGray,
    margin: 10,
    textAlign: 'center'
  },
  valueStyle: {
    color: Colors.Gray
  },
  iconCont: {
    alignItems: 'flex-start',
    padding: 15
  },
  icon: {
    fontSize: 24,
    marginRight: 15,
    fontWeight: 'bold',
    color: Colors.black54
  },
  button: {
    backgroundColor: Colors.DarkGray,
    margin: 25,
  },
  imageStyle: {
    width: 80,
    height: 80,
    margin: 20,
  },
  imageStyle2: {
    width: 120,
    height: 120,
    margin: 10
  },
  imageStyle3: {
    flex: 1,
    width: 200,
    height: 200,
    margin: 20
  }
})


