import React, { Component } from 'react';
import { View, Text, StyleSheet,Image,AsyncStorage } from 'react-native';
import { Button } from 'native-base';

import Colors from '../common/Colors';

export default class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Image style={styles.imageStyle} source={require('../../../assets/icons/logo-gold.png')} />
        <Text style={styles.textStyle}> Welcome to Originally Guaranteed </Text>
        <View style={styles.buttonsContainer}>
          <Button
            style={styles.button}
            block 
            onPress={() => this.props.navigation.navigate('Login')}
          >
            <Text style={styles.textButton}>Login</Text>
          </Button>
          <Button
            style={styles.button2}
            block 
            onPress={() => this.props.navigation.navigate('Register')}
          >
            <Text style={styles.textButton2}>Signup</Text>
          </Button>

        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    flex: 1,
    alignItems: 'center',
  },
  textStyle: {
    fontSize: 18,
    color: Colors.DarkGray,
    marginHorizontal: 20,
  },
  imageStyle: {
    width: 250,
    height: 250,
    marginTop: '30%'
  },
  buttonsContainer: {
    flex: 1,
    width: '100%',
    padding: 20,
    marginTop: 80,
  },
  button: {
    backgroundColor:Colors.DarkGray,
    marginBottom: 15
  },
  button2: {
    backgroundColor: Colors.white,
    borderColor:Colors.DarkGray,
    borderWidth: 2
  },
  textButton: {
    color: Colors.white
  },
  textButton2: {
    color: Colors.DarkGray
  }
})
