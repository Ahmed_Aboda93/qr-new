import {AsyncStorage} from 'react-native';
import {createStore, applyMiddleware} from 'redux';
import rootReducer from '../reducers';
import thunk from 'redux-thunk';
import {persistReducer,persistStore} from 'redux-persist'


const persistConfig={
    key:'root',
    storage:AsyncStorage,
    whitelist:['auth']
}

const persistedReducer= persistReducer(persistConfig,rootReducer)

export const store = createStore(persistedReducer,{},applyMiddleware(thunk))


export const persisedtStore = persistStore(store)