import React from "react";
import {View,Image,SafeAreaView,ScrollView,Text,TouchableOpacity,Alert,AsyncStorage} from "react-native";
import { Header, Container, Content } from 'native-base';
import  Colors  from "./common/Colors";
import IconComponent from 'react-native-vector-icons/MaterialCommunityIcons'; 
import IconComponent2 from 'react-native-vector-icons/AntDesign'; 
import IconComponent3 from 'react-native-vector-icons/SimpleLineIcons'; 
import {createSwitchNavigator,createAppContainer,createStackNavigator,createDrawerNavigator,DrawerItems} from "react-navigation";

import Launch from './pages/Launch';
import Splash from './pages/Splash';
import Login from './pages/Login';
import Register from './pages/Register';
import Home from './pages/Home';
import result from './pages/result';

import Scan from './pages/Scan';
import CodesList from './pages/CodesList';
import SingleCode from './pages/SingleCode';


// ---------------- Auth Stack ----------------
const AuthStack = createAppContainer(
    createStackNavigator(
      {
        Splash: {
          screen: Splash,
          headerMode: "none",
          navigationOptions: { header: null }
      },
        Login: {
            screen: Login,
            navigationOptions: {
                title: 'Login'
            }
        },
        Register: {
            screen: Register,
            navigationOptions: {
                title: 'Register'
            }
        },
      },
      {
        defaultNavigationOptions: {
          headerStyle: {
            backgroundColor: Colors.DarkGray,
            shadowColor: Colors.black54,
            shadowOffset: { width: 0, height: 0 },
            shadowOpacity: 0.8,
            shadowRadius: 2,
            elevation: 1
          },
          headerTintColor: Colors.white,
          headerTitleStyle: { fontWeight: "bold", fontSize: 16 },
          headerBackTitle: null
        },
        initialRouteName: "Splash"
      }
    )
  );
  // ---------------- Auth Stack ----------------
const ScanStack = createAppContainer(
  createStackNavigator(
    {
      Home: {
        screen: Home,
        headerMode: "none",
        navigationOptions: { header: null }
    },
      Scan: {
        screen: Scan,
        headerMode: "none",
        navigationOptions: { header: null }
    },
      result: {
          screen: result,
          headerMode: "none",
          navigationOptions: { header: null }
      },
    },
    {
      defaultNavigationOptions: {
        headerStyle: {
          backgroundColor: Colors.DarkGray,
          shadowColor: Colors.black54,
          shadowOffset: { width: 0, height: 0 },
          shadowOpacity: 0.8,
          shadowRadius: 2,
          elevation: 1
        },
        headerTintColor: Colors.white,
        headerTitleStyle: { fontWeight: "bold", fontSize: 16 },
        headerBackTitle: null
      },
      initialRouteName: "Home"
    }
  )
);
// ---------------- Auth Stack ----------------
const CodesStack = createAppContainer(
  createStackNavigator(
    {
      CodesList: {
        screen: CodesList,
        headerMode: "none",
        navigationOptions: { header: null }
      },
      SingleCode: {
        screen: SingleCode,
        headerMode: "none",
        navigationOptions: { header: null }
      }
    },
    {
      defaultNavigationOptions: {
        headerStyle: {
          backgroundColor: Colors.DarkGray,
          shadowColor: Colors.black54,
          shadowOffset: { width: 0, height: 0 },
          shadowOpacity: 0.8,
          shadowRadius: 2,
          elevation: 1
        },
        headerTintColor: Colors.white,
        headerTitleStyle: { fontWeight: "bold", fontSize: 16 },
        headerBackTitle: null
      },
      initialRouteName: "CodesList"
    }
  )
);


  const CustomtDrawerComponent = props => (
    <Container>
      <SafeAreaView style={{ backgroundColor: 'rgba(0, 0, 0, 0.0)' }}/>
      <View style={{ 
        height: 150,
        padding: 20,
        backgroundColor: Colors.white,
        alignItems: 'center',
        justifyContent: 'center',
        }} >
        <Image source={require('../../assets/icons/logo-gold.png')} style={{ width: 120,height: 120}} />
        {/* <Text style={{ marginTop: 10 }}> OG  </Text> */}
      </View>
      <Content>
        <ScrollView contentContainerStyle={{flex: 1,  flexDirection: 'column', justifyContent: 'space-between' }}>
          <DrawerItems {...props} />
          <TouchableOpacity 
          style={{flexDirection: 'row',alignItems: 'center'}}
          onPress={() =>  
            Alert.alert(
              'Do you want to Logout ?',
              'press Logout if you confirm this, if not press cancel',
              [
                {
                  text: 'Cancel',
                  onPress: () => console.log('Cancel Pressed'),
                  style: 'cancel',
                },
                {text: 'Logout', onPress: () => {
                  AsyncStorage.clear();
                  props.navigation.navigate('Login');
                }},
              ],
              {cancelable: false},
            )
          }
          >
            <IconComponent3
              name='logout'
              style={{ color: Colors.black54,fontSize: 22, marginHorizontal: 16,alignItems: 'center' }}
            />
            <Text style={{color: Colors.black87,fontSize: 16,fontWeight: 'bold', margin: 16}}>Log Out</Text>
          </TouchableOpacity>
        </ScrollView>
      </Content>
    </Container>
  );
// ---------------- App Drawer ----------------
const AppDrawer = createAppContainer(
    createDrawerNavigator(
      {
        Home: {
            screen: ScanStack,
            navigationOptions: {
              drawerLabel: "Scan OG code",
              drawerIcon: ({ tintColor }) => (
                <IconComponent2
                  name='scan1'
                  style={{ color: tintColor,fontSize: 24 }}
                />
              )
            }
          },
        Codes: {
          screen: CodesStack,
          navigationOptions: {
            drawerLabel: "My Items",
            drawerIcon: ({ tintColor }) => (
              <IconComponent
                name='shield-outline'
                style={{ color: tintColor,fontSize: 24  }}
              />
            )
          }
        },
  
      },
      {
        initialRouteName: "Home",
        contentComponent: CustomtDrawerComponent,
        contentOptions: { activeTintColor: Colors.AppColor },
        drawerOpenRoute: 'DrawerOpen',
        drawerCloseRoute: 'DrawerClose',
        drawerToggleRoute: 'DrawerToggle'
      }
    )
  );
  

  const RootNavigtor = createAppContainer(
    createSwitchNavigator(
      {
        LaunchScreen: Launch,
        AuthStack: AuthStack,
        AppDrawer: AppDrawer
      },
      {
        initialRouteName: "LaunchScreen"
      }
    )
  );


  
export default RootNavigtor;