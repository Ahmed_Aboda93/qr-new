import React from 'react'
import { StatusBar,View,Text} from 'react-native'
import Router from './Router'
import {Colors} from "./common/Colors";

//global.isRTL = I18nManager.isRTL;

class App extends React.Component {

  constructor(props) {
    super(props)
    StatusBar.setBarStyle('light-content', true,);
    
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar
          barStyle='dark-content'
          // dark-content, light-content and default
          hidden={false}
          //To hide statusBar
          // backgroundColor={Colors.white}
          //Background color of statusBar
          translucent={false}
        />
        <Router />
      </View>
    )
  }
};


export default App;