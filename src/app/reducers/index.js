import { persistCombineReducers } from "redux-persist";
import storage from "redux-persist/es/storage";
import { combineReducers } from "redux";

import auth from "./auth";
import check from "./check";

export default combineReducers({
    auth,
    check

});
