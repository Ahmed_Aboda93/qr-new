import {
  CHECK_ATTEMPT,
  CHECK_SUCCESS,
  CHECK_FAILED
  } from "../actions/ActionTypes";


  let inital_state = {
    check_loading: false,
    check_success               : false,
    check_error_message         : "",
    message                     : null,
    status                      : '',
    title                       : '',
    brand                       : '',
    origin                      : '',
    specs                       : '',
    img                         : '',
    
  };

  export default (state = inital_state, action) => {
    switch (action.type) {
     
      // Login Attemp  >>>>>>>>>>>>>>>>>>>>>
      case CHECK_ATTEMPT:
        return Object.assign({}, state, {
          check_loading          : true,
          check_success          :false,
          check_error_message    :''
        });
  
      case CHECK_SUCCESS:
        return Object.assign({}, state, {
          check_loading           : false,
          check_success           : true,
          check_error_message     : "",
          message                 : action.payload.message,
          status                  : action.payload.status, 
          title                   : action.payload.title, 
          brand                   : action.payload.brand,
          origin                  : action.payload.origin,
          specs                   : action.payload.specs,
          img                     : action.payload.img,
        });
  
      case CHECK_FAILED:
        return Object.assign({}, state, {
          check_loading               : false,
          check_success               : false,
          check_error_message         : "",
          message                     : action.payload.message,
          status                      : action.payload.status,   
        });
  
      default:
        return state;
    }
  };