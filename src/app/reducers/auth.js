import {
    LOGIN_ATTEMPT,
    LOGIN_SUCCESS,
    LOGIN_FAILED,
    LOGOUT,
    CREATE_ACCOUNT_ATTEMPT,
    CREATE_ACCOUNT_SUCCESS,
    CREATE_ACCOUNT_FAILED,
    RESET_AUTH_LOADING,
  } from "../actions/ActionTypes";


  let inital_state = {
    create_user_loading        : false,
    crate_user_success         : false,
    create_user_error_message  : "",
    user_login_loading: false,
    user_login_success               : false,
    user_login_error_message         : "",
    authorized                       :false,
    token                            : null,
    
  };

  export default (state = inital_state, action) => {
    switch (action.type) {
      // Reset Auth Loading >>>>>>>>>>>>>>>>>>>>>
      case RESET_AUTH_LOADING:
      return Object.assign({}, state, {
        create_user_loading        : false,
        crate_user_success         : false,
        create_user_error_message  : "",
        user_login_loading         : false,
        user_login_success         : false,
        user_login_error_message   : "",
      });
      // Create Account  >>>>>>>>>>>>>>>>>>>>>
    case CREATE_ACCOUNT_ATTEMPT:
      return Object.assign({}, state, {
        create_user_loading          : true,
        crate_user_success           : false,
        create_user_error_message    : "",
      });

    case CREATE_ACCOUNT_SUCCESS:
      return Object.assign({}, state, {
        create_user_loading          : false,
        crate_user_success           : true,
        create_user_error_message    : "",
        token                        : action.payload.token,
        authorized                   : true,
      });

    case CREATE_ACCOUNT_FAILED:
      return Object.assign({}, state, {
        create_user_loading          : false,
        crate_user_success           : false,
        create_user_error_message    : action.payload
      });
      // Login Attemp  >>>>>>>>>>>>>>>>>>>>>
      case LOGIN_ATTEMPT:
        return Object.assign({}, state, {
          user_login_loading          : true,
          user_login_success          :false,
          user_login_error_message    :''
        });
  
      case LOGIN_SUCCESS:
        return Object.assign({}, state, {
          user_login_loading           : false,
          user_login_success           : true,
          user_login_error_message     : "",
          token                        : action.payload.token,
          authorized                   : true,   
  
        });
  
      case LOGIN_FAILED:
        return Object.assign({}, state, {
          user_login_loading: false,
          user_login_success: false,
          user_login_error_message: action.payload
        });
  
      case LOGOUT:
      return Object.assign({}, state, {
        user_login_loading: false,
        user_login_success               : false,
        user_login_error_message         : "",
        authorized                       :false,
        token                            : null,
      });

      default:
        return state;
    }
  };