const Colors = {
    AppColor:       '#D6A92D',
    Gray:           'rgba(178,178,178,1)',
    Blue:           '#555e60',
    DarkGray:       '#333333',
    LightGray:      'rgba(207,206,204,0.5)',
    Red:            'rgba(208,83,67,1)',
    Green:          'rgba(55,203,123,1)',
    DarkBlue:       'rgba(17,107,213,1)',
    MenuLeft:       '#fafafa',
    Black:          'rgb(51, 58, 66)',
    mainBlue:       '#9b195d',
    mainRed:        '#9b195d',
    mainGreen:      '#3DB657',
    white:          '#FFFFFF',
    background:     '#F4F4F6',
    inputBg:        '#F1F3F4',
    seprator:       '#FCFDFD',
    black87:        'rgba(0,0,0,.87)',
    black54:        'rgba(0,0,0,.54)',
    black38:        'rgba(0,0,0,.38)',
    black19:        'rgba(0,0,0,.19)',
    black11:        'rgba(0,0,0,.11)',
    black8:         'rgba(0,0,0,.08)',
    statusBarbg:    '#FFA64D',
    error:          '#FF473D',
    note:           '#ffd54f'
  }
  
  export default Colors
  